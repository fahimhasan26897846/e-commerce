<?php
session_start();
require_once '../config/connect.php';
if(!isset($_SESSION['email']) & empty($_SESSION['email'])){
    header('location: login.php');
}
if(isset($_GET) && !empty($_GET))
{
    $id=$_GET['id'];

}
else
    {
        header('location: catagories.php');
    }

if(isset($_POST) & !empty($_POST))
{
    $id = mysqli_real_escape_string($connect, $_POST['id']);
    $name = mysqli_real_escape_string($connect,$_POST['catagoryname']);
    $sql="UPDATE catagory SET name= '$name' WHERE id=$id";
    $result =mysqli_query($connect, $sql);
    if($result){
        $crrmsg = "Catagory Updated successfully";
    }
    else
    {
        $errmsg = "failed to Update catagory";
    }
}
?>
<?php include ('inc/header.php');?>
<?php include ('inc/nav.php')?>
    <section id="content">
        <div class="content-blog">
            <div class="container">
                <?php
                if(isset($crrmsg)){
                    echo "<div class='alert alert-success text-center' role='alert'>$crrmsg</div>";
                }elseif (isset($errmsg)){
                    echo "<div class='alert alert-danger text-center' role='alert'>$errmsg</div>";
                }
                ?>
                <form action="" method="post">
                    <?php
                    $sql="SELECT * FROM catagory WHERE id='$id'";
                    $result=mysqli_query($connect, $sql);
                    $r = mysqli_fetch_assoc($result);
                    ?>
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                        <label for="Productname">Catagory Name</label>
                        <input type="text" class="form-control" name="catagoryname" id="Catagoryname"
                               placeholder="Catagory Name" value="<?php echo $r['name']?>">
                    </div>
                    <button type="submit" class="btn btn-default">SUBMIT</button>
                </form>
            </div>
        </div>
    </section>
<?php include ('inc/footer.php')?>