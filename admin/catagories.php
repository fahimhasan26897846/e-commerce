<?php
session_start();
require_once '../config/connect.php';
if(!isset($_SESSION['email']) & empty($_SESSION['email'])){
    header('location: login.php');
}
$sql="SELECT * FROM catagory";
$result=mysqli_query($connect, $sql);
?>
<?php include ('inc/header.php');?>
<?php include ('inc/nav.php')?>
    <section id="content">
        <div class="content-blog">
            <div class="container">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Catagory Name</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while ($r= mysqli_fetch_assoc($result)) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $r['id']?></th>
                            <td><?php echo $r['name'] ?></td>
                            <td><a href="editcatagory.php?id=<?php echo $r['id']?>">Edit</a> | <a href="deletecatagory.php?id=<?php echo  $r['id'] ?>">Delete</a></td>
                        </tr>
                        <?php
                    }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
<?php include ('inc/footer.php')?>