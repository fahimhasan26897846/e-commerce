<?php
session_start();
require_once '../config/connect.php';
if(!isset($_SESSION['email']) & empty($_SESSION['email'])){
    header('location: login.php');
}
if(isset($_POST['catagoryname']) & !empty($_POST['catagoryname']))
{
    $name = mysqli_real_escape_string($connect,$_POST['catagoryname']);
    $sql="INSERT INTO catagory (name) VALUE ('$name')";
    $result =mysqli_query($connect, $sql);
    if($result){
        $crrmsg = "Catagory Added successfully";
    }
    else
        {
          $errmsg =  "failed to add catagory";
        }
}
?>
<?php include ('inc/header.php');?>
<?php include ('inc/nav.php')?>
    <section id="content">
        <div class="content-blog">
            <div class="container">
                <?php
                if(isset($crrmsg)){
                    echo "<div class='alert alert-success text-center' role='alert'>$crrmsg</div>";
                }elseif (isset($errmsg)){
                    echo "<div class='alert alert-danger text-center' role='alert'>$errmsg</div>";
                }
                ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="Productname">Catagory Name</label>
                        <input type="text" class="form-control" name="catagoryname" id="Catagoryname"
                               placeholder="Catagory Name">
                    </div>
                    <button type="submit" class="btn btn-default">SUBMIT</button>
                </form>
            </div>
        </div>
    </section>
<?php include ('inc/footer.php')?>